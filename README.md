# agh-targi-pracy-2-backend

## Środowiska

**Development** spięte z gałęzią `development`:

    backend-development.agh-tapcy2.enl-projects.com

**Production** spięte z gałęzią `main`:

    backend.agh.enl-projects.com

## Spis treści

- [Endpointy](#endpointy)
    - [Użytkownicy](#endpointy-użytkownicy)
    - [Wystawcy](#endpointy-wystawcy)
    - [Wydarzenia](#endpointy-wydarzenia)
    - [Ogłoszenia pracy](#endpointy-ogłoszenia-pracy)
    - [Podania o pracę](#endpointy-podania-o-pracę)
- [Modele danych](#modele-danych)
    - [Użytkownicy](#modele-danych-użytkownicy)
    - [Wystawcy](#modele-danych-wystawcy)
    - [Wydarzenia](#modele-danych-wydarzenia)
    - [Ogłoszenia pracy](#modele-danych-ogłoszenia-pracy)
    - [Podania o pracę](#modele-danych-podania-o-pracę)
- [Modele danych - dodatkowe](#modele-danych-dodatkowe)



## Endpointy

[⬆️ spis treści](#spis-treści)


#### Poziomy dostępu:

Endpointy objęte autoryzacją przez `token ` oznaczono ikoną 🛂 podając jednocześnie poziom dostępu:

Access Level | Zastosowanie | Opis
--- | --- | ---
🛂 <b>user</b> | Studenci |  `accountType:` `"user"`
🛂 <b>admin</b> | Wystawcy |  `accountType:` `"admin"`
🛂 <b>superadmin</b> | Organizatorzy (Centrum Karier AGH etc.) | `accountType:` `"superadmin"`
🛂 <b>staff or superadmin</b> | Pracownicy danej firmy-wystawcy | staff: `user` o danym `:exhibitorId`
🛂 <b>self or superadmin</b> | Własne konto (ten user) | self: `user` o `_id` równym `:userId`

### Endpointy: Użytkownicy

#### `/users` `GET`

Zwraca wszystkich userów. 

🛂 superadmin

#### `/users` `POST`

Rejestracja nowego usera. 

Musi zawierać `username` i `password`.

⚠️ `username` przechowuje email.

#### `/users/login` `POST`

Logowanie.

Musi zawierać `username` i `password`.

Zwraca Bearer `token` do autoryzacji (expires in: 1h).

⚠️ `username` przechowuje email.

Przykładowa odpowiedź:

```javascript
{
    "status": "success",
    "data": {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MGRkYWZlNjI4MjA1MzZmYjRmMmIyNTAiLCJhY2NvdW50VHlwZSI6IlNVUEVSQURNSU4iLCJpYXQiOjE2MjU3NjU0NzksImV4cCI6MTYyNTc2OTA3OX0.MfI3K-L_Dtlu5I-7YTPgI8kq6uYn0b4UuWl_oRoGqhM"
    },
    "message": null
}
```

#### `/users/whoami` `GET`

Zwraca zalogowanego usera, bez hash.

Wymaga `token`.

#### `/users/:userId` `PUT`

Aktualizacja usera o ID `:userId`.

🛂 self or superadmin

⚠️  Nie ma możliwości zmiany `accountType`.

⚠️  Nie ma możliwości zmiany `password`.

#### `/users/:userId/password` `PUT`

Zmiana hasła przez nadpisanie przesłanym.

Musi zawierać `password`.

🛂 self or superadmin

#### `/users/reset-password/:username` `POST`

Reset hasła dla użytkownika o podanym emailu/username i wysłanie nowego hasła mailem.

#### `/users/:userId` `DELETE`

Usuwanie usera o ID `:userId`.

🛂 superadmin

#### `/users/register-exhibitor-user` `POST`

Rejestruje nowe konto dla wystawcy z dostępem do panelu admina.

🛂 superadmin

Należy przesłać taki komplet danych:

```javascript
{
    "username": "pawel@enlighten.pl",
    "password": "ADMINADMIN",
    "firstName": "Jan",
    "lastName": "Kowalski",
    "exhibitorId": "60e57e54c7099b3c54cfa9dd"
}
```

#### `/users/:userId/cv` `POST`

🛂 self or superadmin

oczekuje Key: `file`
opcjonalnie: `name`

dodaje kolejny element na koniec tablicy `cvs` użytkownika o ID `:userId`.

#### `/users/:userId/cv/:cvId` `DELETE`

🛂 self or superadmin

"Usuwa" (oznacza `isDeleted: true`) CV o ID `:cvId` użytkownika o ID `:userId`.

### Endpointy: Wystawcy

#### `/exhibitors` `GET`

🛂 public

#### `/exhibitors` `POST`

🛂 superadmin

#### `/exhibitors/:exhibitorId` `GET`

🛂 public

#### `/exhibitors/:exhibitorId` `PUT`

🛂 staff or superadmin

#### `/exhibitors/:exhibitorId` `DELETE`

🛂 superadmin

#### `/exhibitors/:exhibitorId/logo` `POST`

🛂 staff or superadmin

oczekuje Key: `file`,

dodaje (lub nadpisuje) logo wystawcy o ID `:exhibitorId`.

#### `/exhibitors/:exhibitorId/logo` `DELETE`

🛂 staff or superadmin

usuwa logo wystawcy o ID `:exhibitorId`.

#### `/exhibitors/:exhibitorId/images` `POST`

🛂 staff or superadmin

oczekuje Key: `file`

dodaje kolejny element na koniec tablicy `images` wystawcy o ID `:exhibitorId`.

Przykład - screenshot (uwaga, nieaktualny, odnosił się do `meetUsLinks` (które są obsolete)):

<a href="https://ibb.co/23DS4Qf"><img src="https://i.ibb.co/CKk6GZT/meetuslink.jpg" alt="meetuslink" border="0"></a>

#### `/exhibitors/:exhibitorId/iamges/:imageId` `DELETE`

🛂 staff or superadmin

usuwa obrazek o ID `:imageId` u wystawcy o ID `:exhibitorId`.

### Endpointy: Wydarzenia

Brakujące README

### Endpointy: Ogłoszenia pracy

#### `/job-offers` `GET`

🛂 user

#### `/job-offers` `POST`

🛂 admin

#### `/job-offers/:jobId` `GET`

🛂 user

#### `/job-offers/:jobId` `PUT`

🛂 admin

#### `/job-offers/:jobId` `DELETE`

🛂 admin

### Endpointy: Podania o pracę

#### `/job-applications/:exhibitorId` `GET`

zwraca wszystkie job-applications wysłane do wystawcy o ID `:exhibitorId`

🛂 staff or superadmin

#### `/job-applications/user/:userId` `GET`

zwraca wszystkie job-applications usera o ID `:userId`

🛂 self or superadmin

#### `/job-applications/` `POST`

tworzy nowe job-application

Musi zawierać: `userId`, `cvId` i `exhibitorId`.

Opcjonalnie: `jobOfferId`, `linkedIn`, `message`


przykładowe zapytanie:

```javascript
{
    "userId": "6178add09a29f602a86e1f8f",
    "exhibitorId": "6179d07cce9438e06ac1af17",
    "jobOfferId": "61797b5278c54d6217aa6d2a",
    "cvId": "2137420edefb9261jp2p2137",
    "linkedIn": "https://pl.linkedin.com/",
    "message": "test"
}
```

przykładowa odpowiedź:

```javascript
{
  status: 'success',
  data: {
    userId: '6179d07bce9438e06ac1af08',
    exhibitorId: '6179d07cce9438e06ac1af17',
    jobOfferId: '6179d07fce9438e06ac1af4d',
    cvId: '6179d07bce9438e06ac1af09',
    cvUrl: 'user02cv.url',
    userFreeze: {
      firstName: 'Jan',
      lastName: 'Kowalski',
      studentInfo: [Object],
      email: 'username02',
      phone: '000000000'
      linkedIn: 'https://pl.linkedin.com/',
    },
    message: 'test',
    _id: '6179d080ce9438e06ac1af63',
    __v: 0
  },
  message: 'job application successfully created.'
}
```

#### `/job-applications/:userId/:applicationId` `PUT`

🛂 self or superadmin

#### `/job-applications/:userId/:applicationId` `DELETE`

🛂 self or superadmin

#### `/job-applications/exhibitor/count-per-single` `GET`

🛂 superadmin

Zwraca listę wystawców oraz odpowiadającą im liczbę zgłoszonych ofert pracy per wystawca w następującej strukturze:

```javascript
[
    {
        "exhibitorId": "616e7cf2ba2a98d6a55b6437",
        "exhibitorName": "Przykładowy wystawca",
        "applicationsCount": 17
    },
    {
        "exhibitorId": "616eb202daa2bc812f7d67c8",
        "exhibitorName": "Skillspace",
        "applicationsCount": 0
    },
]
```

## Modele danych

[⬆️ spis treści](#spis-treści)

### Modele danych - Użytkownicy

```javascript
userCustomSchema: {
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    phone: {
        type: String,
        default: ''
    },
    studentInfo: {
        status: {
            type: String,
            default: null
        },
        major: {
            type: String,
            default: null
        }    
    },
    isBlocked: {
        type: Boolean,
        default: false
    },
    adminOfChannel: {
        type: String,
        default: null
    },
    exhibitorCompany: {
        type: String,
        default: null
    },
    exhibitorId: {
        type: String,
        default: null
    },
    cvs: [
            {
                key: {
                    type: String,
                    default: null,
                },
                url: {
                    type: String,
                    default: null,
                },
                name: {
                    type: String,
                    default: null,
                },
                isDeleted: {
                    type: Boolean,
                    default: false,
                },
            },
        ],
    linkedIn: {
        type: String,
        default: null,
    },
}
```

### Modele danych - Wystawcy

```javascript
const ExhibitorSchema = new Schema({
    name: {
        type: String,
        default: null,
    },
    slug: {
        type: String,
        default: null,
    },
    logo: ImageSchema,
    tags: [String],
    workplaces: [{name: String}],
    desc: {
        type: String,
        default: null,
    },
    shortDesc: {
        type: String,
        default: null,
    },
    fundraiser: {
        type: Boolean,
        default: null,
    },
    websites: [{url: String, name: String}],
    socialMedia: {
        facebook: String,
        instagram: String,
        youtube: String,
        tiktok: String,
        twitter: String,
        other: String,
        linkedIn: String,
    },
    calendly: {
        type: String,
        default: null,
    },
    appointment: {
        teamsUrl: {type: String, default: null},
        calendarUrl: {type: String, default: null},
        calendarCode: {type: String, default: null},
    },
    youtubeVideos: [{url: String}],
});
```

### Modele danych - Wydarzenia

```javascript
const EventSchema = new Schema({
    title: {
        type: String,
        default: null,
    },
    host: {
        type: String,
        default: null,
    },
    slug: {
        type: String,
        default: null,
    },
    exhibitorId: {
        type: Schema.Types.ObjectId,
        default: null,
    },
    desc: {
        type: String,
        default: null,
    },
    startTime: {
        type: Number,
        default: null,
    },
    finishTime: {
        type: Number,
        default: null,
    },
    isActive: {
        type: Boolean,
        default: false,
    },
    workshops: {
        type: Boolean,
        default: false,
    },
    link: {
        type: String,
        default: null,
    },
    streamyardLink: {
        type: String,
        default: null,
    },
    hostContact: {
        type: String,
        default: null,
    },
});
```

### Modele danych - Ogłoszenia pracy

```javascript
const JobOfferSchema = new Schema({
    title: {
        type: String,
        default: null,
    },
    desc: {
        type: String,
        default: null,
    },
    slug: {
        type: String,
        default: null,
    },
    exhibitorId: {
        type: Schema.Types.ObjectId,
        default: null,
    },
    tags: [String],
    workplace: {
        type: String,
        default: null
    }
});
```

### Modele danych - Podania o pracę

```javascript
const jobApplicationSchema = new Schema({
    userId: {
        type: String,
        default: null,
    },
    exhibitorId: {
        type: String,
        default: null,
    },
    jobOfferId: {
        type: String,
        default: null,
    },
    cvId: {
        type: String,
        default: null,
    },
    cvUrl: {
        type: String,
        default: null,
    },
    userFreeze: {
        firstName: {type: String, default: null},
        lastName: {type: String, default: null},
        studentInfo: {
            status: {type: String, default: null},
            major: {type: String, default: null},
        },
        email: {type: String, default: null},
        phone: {type: String, default: null},
        linkedIn: {type: String, default: null},
    },
    message: {
        type: String,
        default: null,
    },
});
```

---

## Modele danych - dodatkowe

### Obrazy

```javascript
const ImageSchema = new Schema({
    fullSize: s3FileSchema,
    medium: s3FileSchema,
    thumb: s3FileSchema
});
```

### s3File

```javascript
const s3FileSchema = new Schema({
    key: {
        type: String,
        default: null
    },
    url: {
        type: String,
        default: null
    },
});
```