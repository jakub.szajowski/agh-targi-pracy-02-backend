const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index.js');
const should = chai.should();
const expect = chai.expect;
const moment = require('moment');

chai.use(chaiHttp);

let createdUser1Id = null;
let createdUser2Id = null;
let createdExhibitorUserId = null;

let user1Token = null;
let user2Token = null;

let createdUser2cvId = null;

let createdExhibitor1Id = null;

let createdEvent1Id = null;

let createdJobOffer1Id = null;

let createdJobApplication1Id = null;
let createdJobApplication2Id = null;

let admin1Id = null;
let admin1Token = null;

describe('API Home endpoint', function () {
    it('should return A TEAPOT HTTP STATUS on / GET', function (done) {
        chai.request(server)
            .get('/')
            .end(function (err, res) {
                expect(res).to.have.status(418);
                done();
            });
    });
});

describe('API Users endpoint', () => {
    it('should register & login USER01 on /users/:userId POST', done => {
        chai.request(server)
            .post('/users')
            .send({
                username: 'user01',
                password: '00001111',
            })
            .end((err, res) => {
                createdUser1Id = res.body.data._id;
                chai.request(server)
                    .post('/users/login')
                    .send({
                        username: 'user01',
                        password: '00001111',
                    })
                    .end((err, res) => {
                        user1Token = res.body.data.token;
                        expect(res).to.have.status(200);
                        expect(res.body.status).to.equal('success');
                        expect(res.body.data).to.be.an('object');
                        done();
                    });
            });
    });

    it('should register & login USER02 on /users/:userId POST', done => {
        chai.request(server)
            .post('/users')
            .send({
                username: 'user02',
                password: '00002222',
                firstName: 'Jan',
                lastName: 'Kowalski',
                phone: '000000000',
                cvs: [{url: 'user02cv.url'}],
            })
            .end((err, res) => {
                createdUser2Id = res.body.data._id;
                createdUser2cvId = res.body.data.cvs[0]._id;
                chai.request(server)
                    .post('/users/login')
                    .send({
                        username: 'user02',
                        password: '00002222',
                    })
                    .end((err, res) => {
                        user2Token = res.body.data.token;
                        expect(res).to.have.status(200);
                        expect(res.body.data).to.be.an('object');
                        done();
                    });
            });
    });

    it('should login as SUPERADMIN on /users/:userId POST', done => {
        chai.request(server)
            .post('/users/login')
            .send({
                username: 'admin01',
                password: '0000',
            })
            .end((err, res) => {
                admin1Token = res.body.data.token;
                chai.request(server)
                    .get('/users/whoami')
                    .set('Authorization', 'Bearer ' + admin1Token)
                    .end((err, res) => {
                        admin1Id = res.body.data._id;
                        expect(res).to.have.status(200);
                        expect(res.body.status).to.equal('success');
                        expect(res.body.data).to.be.an('object');
                        done();
                    });
            });
    });
});

describe('API Exhibitors endpoint', function () {
    it('should create a new exhibitor on /exhibitors POST', function (done) {
        chai.request(server)
            .post('/exhibitors')
            .send({
                name: 'exhibitor@/!01',
                industry: 'metallurgy',
                tags: ['foundry', 'metallurgy', 'engineering'],
            })
            .set('Authorization', 'Bearer ' + admin1Token)
            .end(function (err, res) {
                createdExhibitor1Id = res.body.data._id;
                expect(res).to.have.status(200);
                expect(res.body.data.slug).to.equal('exhibitor01');
                done();
            });
    });

    it('should update EXHIBITOR01 as ADMIN01 on /exhibitors/:exhibitorId PUT', done => {
        chai.request(server)
            .put(`/exhibitors/${createdExhibitor1Id}`)
            .send({
                tags: ['simulations', 'research'],
            })
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data.tags.length).to.equal(2);
                done();
            });
    });

    it('should NOT update EXHIBITOR01 as USER01 on /exhibitors/:exhibitorId PUT', done => {
        chai.request(server)
            .put(`/exhibitors/${createdExhibitor1Id}`)
            .send({
                industry: '',
            })
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(403);
                expect(res.body).to.be.an('object');
                expect(res.body).to.be.empty;
                done();
            });
    });

    it('should assign USER01 as EXHIBITOR01 staff on /users/:userId PUT', done => {
        chai.request(server)
            .put(`/users/${createdUser1Id}`)
            .send({
                exhibitorId: createdExhibitor1Id,
            })
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data.exhibitorId).to.equal(createdExhibitor1Id);
                done();
            });
    }).timeout(500);

    it('should update EXHIBITOR01 as USER01 on /exhibitors/:exhibitorId PUT', done => {
        chai.request(server)
            .put(`/exhibitors/${createdExhibitor1Id}`)
            .send({
                short_desc: 'very serious business.',
            })
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data.tags.length).to.equal(2);
                done();
            });
    });
});

describe('API Events endpoint', function () {
    it('should NOT create a new event as USER01 on /events POST', function (done) {
        chai.request(server)
            .post(`/events`)
            .send({
                title: 'testing',
            })
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(403);
                expect(res.body).to.be.an('object');
                expect(res.body).to.be.empty;
                expect(res.body.data).not.to.exist;
                expect(res.text).to.equal('Forbidden');
                done();
            });
    });

    //NEW EVENT
    it('should create a new event as ADMIN01 on /events POST', function (done) {
        chai.request(server)
            .post('/events')
            .send({
                title: 'testing',
                host: createdUser1Id,
                exhibitorId: createdExhibitor1Id,
                desc: 'this is a POST test',
                workshops: false,
            })
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');

                expect(res.body.data.title).to.equal('testing');
                expect(res.body.data.exhibitorId).to.equal(createdExhibitor1Id);
                expect(res.body.data.isActive).to.equal(false);
                createdEvent1Id = res.body.data._id;
                done();
            });
    });

    it('should NOT activate EVENT01 on /events/handle-active GET', function (done) {
        chai.request(server)
            .get(`/events/handle-active`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');

                expect(res.body.data).to.equal(true);
                expect(res.body.message).to.equal('Number of affected events: 0');
                done();
            });
    });

    it('should chceck if EVENT01 is not active on /events/:eventId GET', function (done) {
        chai.request(server)
            .get(`/events/${createdEvent1Id}`)
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');

                expect(res.body.data.isActive).to.equal(false);
                done();
            });
    });
    //SET TIME
    describe('Boundary conditions: startTime', function () {
        it('should update EVENT01 startTime: in 16 minutes on /events/:eventId PUT', function (done) {
            chai.request(server)
                .put(`/events/${createdEvent1Id}`)
                .send({
                    desc: 'this is a PUT test 1',
                    startTime: moment().add(16, 'minutes').valueOf(),
                    finishTime: moment().add(2, 'hours').valueOf(),
                })
                .set('Authorization', 'Bearer ' + admin1Token)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body.status).to.equal('success');

                    expect(res.body.data.desc).to.equal('this is a PUT test 1');
                    expect(res.body.data.startTime).to.be.a('number');
                    expect(res.body.data.finishTime).to.be.a('number');
                    done();
                });
        });

        it('should NOT activate EVENT01 on /events/handle-active GET', function (done) {
            chai.request(server)
                .get(`/events/handle-active`)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body.status).to.equal('success');

                    expect(res.body.data).to.equal(true);
                    expect(res.body.message).to.equal('Number of affected events: 0');
                    done();
                });
        });

        it('should chceck if EVENT01 is not ative on /events/:eventId GET', function (done) {
            chai.request(server)
                .get(`/events/${createdEvent1Id}`)
                .set('Authorization', 'Bearer ' + user1Token)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body.status).to.equal('success');

                    expect(res.body.data.isActive).to.equal(false);
                    done();
                });
        });

        it('should not be active at startTime: in 5 minutes', function (done) {
            chai.request(server)
                .put(`/events/${createdEvent1Id}`)
                .send({
                    startTime: moment().add(5, 'minutes').valueOf(),
                })
                .set('Authorization', 'Bearer ' + admin1Token)
                .end((err, res) => {
                    chai.request(server)
                        .get(`/events/handle-active`)
                        .end((err, res) => {
                            chai.request(server)
                                .get(`/events/${createdEvent1Id}`)
                                .set('Authorization', 'Bearer ' + user1Token)
                                .end((err, res) => {
                                    expect(res).to.have.status(200);
                                    expect(res.body).to.be.an('object');
                                    expect(res.body.status).to.equal('success');

                                    expect(res.body.data.isActive).to.equal(true);
                                    done();
                                });
                        });
                });
        });

        it('should be active at startTime: now', function (done) {
            chai.request(server)
                .put(`/events/${createdEvent1Id}`)
                .send({
                    startTime: moment().valueOf(),
                })
                .set('Authorization', 'Bearer ' + admin1Token)
                .end((err, res) => {
                    chai.request(server)
                        .get(`/events/handle-active`)
                        .end((err, res) => {
                            chai.request(server)
                                .get(`/events/${createdEvent1Id}`)
                                .set('Authorization', 'Bearer ' + user1Token)
                                .end((err, res) => {
                                    expect(res).to.have.status(200);
                                    expect(res.body).to.be.an('object');
                                    expect(res.body.status).to.equal('success');
                                    expect(res.body.data.isActive).to.equal(true);
                                    done();
                                })
                                .timeout(5000);
                        })
                        .timeout(5000);
                })
                .timeout(5000);
        });
    });

    describe('Boundary conditions: finishTime', function () {
        it('should be active at finishTime: now', function (done) {
            chai.request(server)
                .put(`/events/${createdEvent1Id}`)
                .send({
                    startTime: moment().subtract(2, 'hours').valueOf(),
                    finishTime: moment().subtract(14, 'minutes').valueOf(),
                })
                .set('Authorization', 'Bearer ' + admin1Token)
                .end((err, res) => {
                    chai.request(server)
                        .get(`/events/handle-active`)
                        .end((err, res) => {
                            chai.request(server)
                                .get(`/events/${createdEvent1Id}`)
                                .set('Authorization', 'Bearer ' + user1Token)
                                .end((err, res) => {
                                    expect(res).to.have.status(200);
                                    expect(res.body).to.be.an('object');
                                    expect(res.body.status).to.equal('success');
                                    expect(res.body.data.isActive).to.equal(true);
                                    done();
                                });
                        });
                });
        });

        it('should be active at finishTime: 14 minutes ago', function (done) {
            chai.request(server)
                .put(`/events/${createdEvent1Id}`)
                .send({
                    startTime: moment().subtract(2, 'hours').valueOf(),
                    finishTime: moment().subtract(14, 'minutes').valueOf(),
                })
                .set('Authorization', 'Bearer ' + admin1Token)
                .end((err, res) => {
                    chai.request(server)
                        .get(`/events/handle-active`)
                        .end((err, res) => {
                            chai.request(server)
                                .get(`/events/${createdEvent1Id}`)
                                .set('Authorization', 'Bearer ' + user1Token)
                                .end((err, res) => {
                                    expect(res).to.have.status(200);
                                    expect(res.body).to.be.an('object');
                                    expect(res.body.status).to.equal('success');
                                    expect(res.body.data.isActive).to.equal(true);
                                    done();
                                });
                        });
                });
        });

        it('should NOT be active at finishTime: 15 minutes ago', function (done) {
            chai.request(server)
                .put(`/events/${createdEvent1Id}`)
                .send({
                    finishTime: moment().subtract(15, 'minutes').valueOf(),
                })
                .set('Authorization', 'Bearer ' + admin1Token)
                .end((err, res) => {
                    chai.request(server)
                        .get(`/events/handle-active`)
                        .end((err, res) => {
                            chai.request(server)
                                .get(`/events/${createdEvent1Id}`)
                                .set('Authorization', 'Bearer ' + user1Token)
                                .end((err, res) => {
                                    expect(res).to.have.status(200);
                                    expect(res.body).to.be.an('object');
                                    expect(res.body.status).to.equal('success');
                                    expect(res.body.data.isActive).to.equal(false);
                                    done();
                                });
                        });
                });
        });
    });
});

describe('API Job Offers endpoint', function () {
    it('should NOT get any job offers as unauthorized on /job-offers GET', function (done) {
        chai.request(server)
            .get(`/job-offers`)
            .end((err, res) => {
                expect(res).to.have.status(401);
                expect(res.body).to.be.an('object');
                expect(res.body).to.be.empty;
                expect(res.body.data).not.to.exist;
                expect(res.text).to.equal('Unauthorized');
                done();
            });
    });

    it('should NOT create a new job offer as USER01 on /job-offers POST', function (done) {
        chai.request(server)
            .post(`/job-offers`)
            .send({
                title: 'testing',
            })
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(403);
                expect(res.body).to.be.an('object');
                expect(res.body).to.be.empty;
                expect(res.body.data).not.to.exist;
                expect(res.text).to.equal('Forbidden');
                done();
            });
    });

    //NEW JOB OFFER
    it('should create a new Job Offer as ADMIN01 on /job-offers POST', function (done) {
        chai.request(server)
            .post('/job-offers')
            .send({
                title: 'some job',
                exhibitorId: createdExhibitor1Id,
                desc: 'this is a POST test',
            })
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data.title).to.equal('some job');
                expect(res.body.data.exhibitorId).to.equal(createdExhibitor1Id);
                expect(res.body.data.slug).to.equal('some-job');
                createdJobOffer1Id = res.body.data._id;
                done();
            });
    });

    it('should update Job Offer as ADMIN01 on /job-offers/:offerId PUT', done => {
        chai.request(server)
            .put(`/job-offers/${createdJobOffer1Id}`)
            .send({
                desc: 'update-works',
            })
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data.desc).to.equal('update-works');
                done();
            });
    });
});

describe('Exhibitors users registration', function () {
    it('should NOT register exhibitor user as unauthorized #1', done => {
        chai.request(server)
            .post(`/users/register-exhibitor-user`)
            .send({
                username: 'test@test.com',
            })
            .end((err, res) => {
                expect(res).to.have.status(401);
                expect(res.body).to.be.an('object');
                expect(res.body).to.be.empty;
                expect(res.body.data).not.to.exist;
                expect(res.text).to.equal('Unauthorized');
                done();
            });
    });

    it('should NOT register exhibitor user as unauthorized #2', done => {
        chai.request(server)
            .post(`/users/register-exhibitor-user`)
            .send({
                username: 'test@test.com',
            })
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(403);
                expect(res.body).to.be.an('object');
                expect(res.body).to.be.empty;
                expect(res.body.data).not.to.exist;
                expect(res.text).to.equal('Forbidden');
                done();
            });
    });

    it('should NOT register exhibitor user with incomplete data', done => {
        chai.request(server)
            .post(`/users/register-exhibitor-user`)
            .send({
                username: 'test@test.com',
                password: 'testpassword',
            })
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(400);
                expect(res.body).to.be.an('object');
                done();
            });
    });

    it('should register exhibitor user with correct data', done => {
        chai.request(server)
            .post(`/users/register-exhibitor-user`)
            .send({
                username: 'test@test.com',
                password: 'testpassword',
                firstName: 'Jan',
                lastName: 'Kowalski',
                exhibitorId: createdExhibitor1Id,
            })
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.message).to.equal('OK');
                createdExhibitorUserId = res.body.data;
                done();
            });
    });
});

describe('API Job Application endpoint', () => {
    it('should create new job applications', done => {
        chai.request(server)
            .post('/job-applications')
            .send({
                userId: createdUser1Id,
                exhibitorId: createdExhibitor1Id,
                jobOfferId: createdJobOffer1Id,
                cvId: '0001',
            })
            .end((err, res) => {
                createdJobApplication1Id = res.body.data._id;
                chai.request(server)
                    .post('/job-applications')
                    .send({
                        userId: createdUser2Id,
                        exhibitorId: createdExhibitor1Id,
                        jobOfferId: createdJobOffer1Id,
                        cvId: createdUser2cvId,
                        linkedIn: 'https://pl.linkedin.com/',
                        message: 'test',
                    })
                    .end((err, res) => {
                        expect(res).to.have.status(200);
                        expect(res.body.data).to.be.an('object');
                        expect(res.body.data).to.have.property('exhibitorId');
                        expect(res.body.data.cvId).to.equal(createdUser2cvId);
                        expect(res.body.data.cvUrl).to.equal('user02cv.url');
                        createdJobApplication2Id = res.body.data._id;
                        done();
                    });
            });
    });

    it('should get job applications to EXHIBITOR01 as ADMIN01 at job-applications/:exhibitorId GET', done => {
        chai.request(server)
            .get(`/job-applications/${createdExhibitor1Id}`)
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.data).to.be.an('array');
                expect(res.body.data.length).to.equal(2);
                done();
            });
    });

    it('should get own job applications as USER02 at job-applications/user/:userId GET', done => {
        chai.request(server)
            .get(`/job-applications/user/${createdUser2Id}`)
            .set('Authorization', 'Bearer ' + user2Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.data).to.be.an('array');
                expect(res.body.data.length).to.equal(1);
                done();
            });
    });

    it('should update job application as USER01', done => {
        chai.request(server)
            .put(`/job-applications/${createdUser1Id}/${createdJobApplication1Id}`)
            .set('Authorization', 'Bearer ' + user1Token)
            .send({
                userId: createdUser1Id,
                jobOfferId: createdJobOffer1Id,
                cvId: '0001',
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body.data).to.be.an('object');
                expect(res.body.data).to.have.property('exhibitorId');
                expect(res.body.data.cvId).to.equal('0001');
                createdJobApplication1Id = res.body.data._id;
                done();
            });
    });

    it('should NOT delete APPLICATION01 of USER01 as USER02', done => {
        chai.request(server)
            .delete(`/job-applications/${createdUser1Id}/${createdJobApplication1Id}`)
            .set('Authorization', 'Bearer ' + user2Token)
            .end((err, res) => {
                expect(res).to.have.status(403);
                expect(res.text).to.equal('Forbidden');
                done();
            });
    });

    it('should NOT delete APPLICATION02 of USER02 as USER01', done => {
        chai.request(server)
            .delete(`/job-applications/${createdUser1Id}/${createdJobApplication2Id}`)
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(401);
                expect(res.body.status).to.equal('error');
                expect(res.body.data).to.be.null;
                expect(res.body.message).to.equal('Unauthorized');
                done();
            });
    });
});

describe('Test finish, database cleanup', () => {
    it('should delete APPLICATION01 as USER01 on /job-applications/:userId/:applicationId DELETE', done => {
        chai.request(server)
            .delete(`/job-applications/${createdUser1Id}/${createdJobApplication1Id}`)
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data).to.be.null;
                done();
            });
    });

    it('should delete APPLICATION02 as USER02 on /job-applications/:userId/:applicationId DELETE', done => {
        chai.request(server)
            .delete(`/job-applications/${createdUser2Id}/${createdJobApplication2Id}`)
            .set('Authorization', 'Bearer ' + user2Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data).to.be.null;
                done();
            });
    });

    it('should delete USER01 on /users/:userId DELETE', done => {
        chai.request(server)
            .delete(`/users/${createdUser1Id}`)
            .set('Authorization', 'Bearer ' + user1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data).to.be.null;
                done();
            });
    });

    it('should delete USER02 on /users/:userId DELETE', done => {
        chai.request(server)
            .delete(`/users/${createdUser2Id}`)
            .set('Authorization', 'Bearer ' + user2Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data).to.be.null;
                done();
            });
    });

    it('should delete EXHHIBITOR_USER1 on /users/:userId DELETE', done => {
        chai.request(server)
            .delete(`/users/${createdExhibitorUserId}`)
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data).to.be.null;
                done();
            });
    });

    it('should delete EVENT01 as ADMIN01 on /exhibitors/:exhibitorId DELETE', done => {
        chai.request(server)
            .delete(`/events/${createdEvent1Id}`)
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data).to.be.null;
                done();
            });
    });

    it('should delete EXHIBITOR01 as ADMIN01 on /exhibitors/:exhibitorId DELETE', done => {
        chai.request(server)
            .delete(`/exhibitors/${createdExhibitor1Id}`)
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                expect(res.body.data).to.be.null;
                done();
            });
    });

    it('should delete Job Offer as ADMIN01 on /job-offers/:offerId DELETE', done => {
        chai.request(server)
            .delete(`/job-offers/${createdJobOffer1Id}`)
            .set('Authorization', 'Bearer ' + admin1Token)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body.status).to.equal('success');
                done();
            });
    });
});
