const logger = require('winston');
const express = require('express');
const router = express.Router();
const jobApplicationController = require('../controllers/jobApplication');
const resH = require('@enlighten1/express-response-helper');
const auth = require('@enlighten1/auth');
const customAccessLevel = require('../middleware/custom-access-level');

router.get(
    '/:exhibitorId',
    auth.jwt,
    customAccessLevel.staffOrSuperadmin,
    async function (req, res) {
        try {
            const data = await jobApplicationController.getByExhibitorId(req.params.exhibitorId);
            resH.send(res, data);
        } catch (error) {
            logger.error(`Error while getting job applications: ${error}`);
            resH.send(res, null, false, 400, `Error while getting job applications: ${error}`);
        }
    }
);

router.get('/user/:userId', auth.jwt, auth.accessLevel.selfOrSuperadmin, async function (req, res) {
    try {
        const data = await jobApplicationController.getByUserId(req.params.userId);
        resH.send(res, data);
    } catch (error) {
        logger.error(`Error while getting job applications: ${error}`);
        resH.send(res, null, false, 400, `Error while getting job applications: ${error}`);
    }
});

router.post('/', async function (req, res) {
    try {
        const result = await jobApplicationController.create(req.body);
        resH.send(res, result.data, result.success, result.status, result.message);
    } catch (error) {
        logger.error(`Unable to create new application: ${error}`);
        resH.send(res, null, false, 400, `Unable to create new application: ${error}`);
    }
});

router.put(
    '/:userId/:applicationId',
    auth.jwt,
    auth.accessLevel.selfOrSuperadmin,
    async function (req, res) {
        try {
            const result = await jobApplicationController.update(
                req.params.userId,
                req.params.applicationId,
                req.body
            );
            resH.send(res, result.data, result.success, result.status, result.message);
        } catch (error) {
            logger.error(`Unable to update exhibitor: ${error}`);
            resH.send(res, null, false, 400, `Unable to update exhibitor: ${error}`);
        }
    }
);

router.delete(
    '/:userId/:applicationId',
    auth.jwt,
    auth.accessLevel.selfOrSuperadmin,
    async function (req, res) {
        try {
            const result = await jobApplicationController.delete(
                req.user._id.valueOf(),
                req.params.applicationId
            );
            resH.send(res, result.data, result.success, result.status, result.message);
        } catch (error) {
            logger.error(`Unable to delete job applications: ${error}`);
            resH.send(res, null, false, 400, `Unable to delete job application: ${error}`);
        }
    }
);

router.get(
    '/exhibitor/count-per-single',
    auth.jwt,
    auth.accessLevel.superadmin,
    async function (req, res) {
        try {
            const data = await jobApplicationController.getNumberPerExhibitor();
            resH.send(res, data);
        } catch (error) {
            logger.error(`Unable to list job applications stats: ${error}`);
            resH.send(res, null, false, 400, `Unable to list job applications stats: ${error}`);
        }
    }
);

module.exports = router;
