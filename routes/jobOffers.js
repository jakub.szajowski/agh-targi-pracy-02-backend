const logger = require('winston');
const express = require('express');
const router = express.Router();
const JobOfferController = require('../controllers/jobOffer');
const resH = require('@enlighten1/express-response-helper');
const auth = require('@enlighten1/auth');

const jobOffersCache = require('../job-offers-cache.json');

router.get('/', async function (req, res) {
    try {
        // const data = await JobOfferController.getAll();
        // resH.send(res, data);
        res.json(jobOffersCache);
    } catch (error) {
        logger.error(`Error while getting all job offers: ${error}`);
        resH.send(res, null, false, 400, `Error while getting all job offers: ${error}`);
    }
});

router.get('/:offerId', auth.jwt, auth.accessLevel.user, async function (req, res) {
    try {
        const data = await JobOfferController.getById(req.params.offerId);
        resH.send(res, data);
    } catch (error) {
        logger.error(`Error while getting single job offer: ${error}`);
        resH.send(res, null, false, 400, `Error while getting single job offer: ${error}`);
    }
});

router.post('/', auth.jwt, auth.accessLevel.admin, async function (req, res) {
    try {
        const data = await JobOfferController.create(req.body);
        resH.send(res, data, true, 200, 'Job offer successfully created.');
    } catch (error) {
        logger.error(`Unable to create new job offer: ${error}`);
        resH.send(res, null, false, 400, `Unable to create new job offer: ${error}`);
    }
});

router.put('/:offerId', auth.jwt, auth.accessLevel.admin, async function (req, res) {
    try {
        const data = await JobOfferController.updateById(req.params.offerId, req.body);
        resH.send(res, data, true, 200, 'Job offer successfully updated.');
    } catch (error) {
        logger.error(`Unable to update job offer: ${error}`);
        resH.send(res, null, false, 400, `Unable to update job offer: ${error}`);
    }
});

router.delete('/:eventId', auth.jwt, auth.accessLevel.admin, async function (req, res) {
    try {
        await JobOfferController.deleteById(req.params.eventId);
        resH.send(res, null, true, 200, 'Job offer successfully deleted.');
    } catch (error) {
        logger.error(`Unable to delete job offer: ${error}`);
        resH.send(res, null, false, 400, `Unable to delete job offer: ${error}`);
    }
});

module.exports = router;
