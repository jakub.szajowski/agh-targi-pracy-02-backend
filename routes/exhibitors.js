const logger = require('winston');
const express = require('express');
const router = express.Router();
const ExhibitorController = require('../controllers/exhibitor');
const customAccessLevel = require('../middleware/custom-access-level');

const resH = require('@enlighten1/express-response-helper');
const auth = require('@enlighten1/auth');

const exhibitorsCache = require('../exhibitors-cache.json');

router.get('/', async function (req, res) {
    try {
        // const data = await ExhibitorController.getAll();
        // resH.send(res, data);
        res.json(exhibitorsCache);
    } catch (error) {
        logger.error(`Error while getting all exhibitors: ${error}`);
        resH.send(res, null, false, 400, `Error while getting all exhibitors: ${error}`);
    }
});

router.get('/:exhibitorId', async function (req, res) {
    try {
        const data = await ExhibitorController.getById(req.params.exhibitorId);
        resH.send(res, data);
    } catch (error) {
        logger.error(`Error while getting single exhibitor: ${error}`);
        resH.send(res, null, false, 400, `Error while getting single exhibitor: ${error}`);
    }
});

router.post('/', auth.jwt, auth.accessLevel.superadmin, async function (req, res) {
    try {
        const data = await ExhibitorController.create(req.body);
        resH.send(res, data, true, 200, 'exhibitor successfully created.');
    } catch (error) {
        logger.error(`Unable to create new exhibitor: ${error}`);
        resH.send(res, null, false, 400, `Unable to create new exhibitor: ${error}`);
    }
});

router.put(
    '/:exhibitorId',
    auth.jwt,
    customAccessLevel.staffOrSuperadmin,
    async function (req, res) {
        try {
            const data = await ExhibitorController.updateById(req.params.exhibitorId, req.body);
            resH.send(res, data, true, 200, 'exhibitor successfully updated.');
        } catch (error) {
            logger.error(`Unable to update exhibitor: ${error}`);
            resH.send(res, null, false, 400, `Unable to update exhibitor: ${error}`);
        }
    }
);

//IMAGES
router.post(
    '/:exhibitorId/logo',
    auth.jwt,
    customAccessLevel.staffOrSuperadmin,
    global.upload.single('file'),
    async function (req, res) {
        try {
            const data = await ExhibitorController.uploadExhibitorLogo(
                req.params.exhibitorId,
                req.file,
                req.body
            );
            resH.send(res, data, true, 200, 'Exhibitor image successfully uploaded');
        } catch (error) {
            logger.error(error);
            resH.send(res, null, false, 400, `Unable to upload post image: ${error}`);
        }
    }
);

router.delete(
    '/:exhibitorId/logo',
    auth.jwt,
    customAccessLevel.staffOrSuperadmin,
    async (req, res) => {
        try {
            await ExhibitorController.deleteExhibitorLogo(req.params.exhibitorId);
            resH.send(res, null, true, 200, 'Image successfully deleted');
        } catch (error) {
            logger.error(error);
            resH.send(res, null, false, 400, `Unable to delete image: ${error}`);
        }
    }
);

router.post(
    '/:exhibitorId/images',
    auth.jwt,
    customAccessLevel.staffOrSuperadmin,
    global.upload.single('file'),
    async function (req, res) {
        try {
            const data = await ExhibitorController.uploadExhibitorImage(
                req.params.exhibitorId,
                req.file
            );
            resH.send(res, data, true, 200, 'Exhibitor image successfully uploaded');
        } catch (error) {
            logger.error(`Unable to upload exhibitor image: ${error}`);
            resH.send(res, null, false, 400, `Unable to upload exhibitor image: ${error}`);
        }
    }
);

router.delete(
    '/:exhibitorId/images/:imageId',
    auth.jwt,
    customAccessLevel.staffOrSuperadmin,
    async function (req, res) {
        try {
            await ExhibitorController.deleteExhibitorImage(
                req.params.exhibitorId,
                req.params.imageId
            );
            resH.send(res, null, true, 200, 'Exhibitor image successfully successfully deleted.');
        } catch (error) {
            logger.error(error);
            resH.send(res, null, false, 400, `Unable to delete exhibitor image: ${error}`);
        }
    }
);

router.delete('/:exhibitorId', auth.jwt, auth.accessLevel.superadmin, async function (req, res) {
    try {
        await ExhibitorController.deleteById(req.params.exhibitorId);
        resH.send(res, null, true, 200, 'exhibitor successfully deleted.');
    } catch (error) {
        logger.error(`Unable to delete exhibitor: ${error}`);
        resH.send(res, null, false, 400, `Unable to delete exhibitor: ${error}`);
    }
});

module.exports = router;
