const logger = require('winston');
const express = require('express');
const router = express.Router();
const EventController = require('../controllers/event');
const resH = require('@enlighten1/express-response-helper');
const auth = require('@enlighten1/auth');

router.get('/', async function (req, res) {
    try {
        const data = await EventController.getAll();
        resH.send(res, data);
    } catch (error) {
        logger.error(`Error while getting all events: ${error}`);
        resH.send(res, null, false, 400, `Error while getting all events: ${error}`);
    }
});

router.get('/handle-active', async function (req, res) {
    try {
        const changed = await EventController.updateActiveEvents();
        resH.send(res, true, true, 200, `Number of affected events: ${changed}`);
    } catch (error) {
        logger.error(`Error while updating isActive events`);
        resH.send(res, null, false, 500);
    }
});

router.get('/:eventId', async function (req, res) {
    try {
        const data = await EventController.getById(req.params.eventId);
        resH.send(res, data);
    } catch (error) {
        logger.error(`Error while getting single event: ${error}`);
        resH.send(res, null, false, 400, `Error while getting single event: ${error}`);
    }
});

router.post('/', auth.jwt, auth.accessLevel.superadmin, async function (req, res) {
    try {
        const data = await EventController.create(req.body);
        resH.send(res, data, true, 200, 'event successfully created.');
    } catch (error) {
        logger.error(`Unable to create new event: ${error}`);
        resH.send(res, null, false, 400, `Unable to create new event: ${error}`);
    }
});

router.put('/:eventId', auth.jwt, auth.accessLevel.admin, async function (req, res) {
    try {
        const data = await EventController.updateById(req.params.eventId, req.body);
        resH.send(res, data, true, 200, 'event successfully updated.');
    } catch (error) {
        logger.error(`Unable to update event: ${error}`);
        resH.send(res, null, false, 400, `Unable to update event: ${error}`);
    }
});

router.delete('/:eventId', auth.jwt, auth.accessLevel.superadmin, async function (req, res) {
    try {
        const data = await EventController.deleteById(req.params.eventId);
        resH.send(res, null, true, 200, 'event successfully deleted.');
    } catch (error) {
        logger.error(`Unable to delete event: ${error}`);
        resH.send(res, null, false, 400, `Unable to delete event: ${error}`);
    }
});

module.exports = router;
