const logger = require('winston');
const express = require('express');
const router = express.Router();
const resH = require('@enlighten1/express-response-helper');
const auth = require('@enlighten1/auth');
const commHelper = require('../helpers/communication-helper');

router.post('/email-to-all-text', auth.jwt, auth.accessLevel.superadmin, async function (req, res) {
    try {
        await commHelper.sendEmailToAllDefault(req.body.title, req.body.text);
        resH.send(res, true);
    } catch (error) {
        logger.error(`Error while sending email to all users: ${error}`);
        resH.send(res, null, false, 400, `Error while sending email to all users: ${error}`);
    }
});

router.post(
    '/email-to-all-template',
    auth.jwt,
    auth.accessLevel.superadmin,
    async function (req, res) {
        try {
            await commHelper.sendEmailToAllTemplate(req.body.title, req.body.templateId);
            resH.send(res, true);
        } catch (error) {
            logger.error(`Error while sending email to all users: ${error}`);
            resH.send(res, null, false, 400, `Error while sending email to all users: ${error}`);
        }
    }
);

router.post('/sms-to-all', auth.jwt, auth.accessLevel.superadmin, async function (req, res) {
    try {
        await commHelper.sendSMSToAll(req.body.text);
        resH.send(res, true);
    } catch (error) {
        logger.error(`Error while sending email to all users: ${error}`);
        resH.send(res, null, false, 400, `Error while sending email to all users: ${error}`);
    }
});

module.exports = router;
