const logger = require('winston');
const express = require('express');
const router = express.Router();
const EventBus = require('@enlighten1/enl-event-bus');
const resH = require('@enlighten1/express-response-helper');
const auth = require('@enlighten1/auth');
const exhibitorModel = require('../models/exhibitor');
const CVController = require('../controllers/cv');

////////////////////////////////////////////////////////////////
// User registration & authorization

router.post('/', async function (req, res) {
    await auth.handleUserRegister(req, res, EventBus);
});

router.post('/login', auth.local, async function (req, res) {
    await auth.handleLogin(req, res, EventBus);
});

router.get('/whoami', auth.jwt, async function (req, res) {
    await auth.handleWhoAmI(req, res);
});

router.put(
    '/:userId/password',
    auth.jwt,
    auth.accessLevel.selfOrSuperadmin,
    async function (req, res) {
        await auth.handleUpdatePassword(req, res, EventBus);
    }
);

////////////////////////////////////////////////////////////////
// User basic CRUD

router.get('/', auth.jwt, auth.accessLevel.superadmin, async function (req, res) {
    await auth.handleGetAllUsers(req, res);
});

router.get('/:userId', auth.jwt, auth.accessLevel.selfOrSuperadmin, async function (req, res) {
    await auth.handleGetUser(req, res);
});

router.put('/:userId', auth.jwt, auth.accessLevel.selfOrSuperadmin, async function (req, res) {
    await auth.handleUpdateUser(req, res, EventBus);
});

router.delete('/:userId', auth.jwt, auth.accessLevel.selfOrSuperadmin, async function (req, res) {
    await auth.handleDeleteUser(req, res, EventBus);
});

////////////////////////////////////////////////////////////////
// Tokens & password reset

router.post('/reset-password/init/:email', async function (req, res) {
    await auth.token.handleResetPasswordInit(req, res, EventBus);
});

router.get('/reset-password/validate-token/:token', async function (req, res) {
    await auth.token.handleTokenVerification(req, res);
});

router.post('/reset-password/finish/:token', async function (req, res) {
    await auth.token.handleTokenUsageAndSetNewPassword(req, res, EventBus);
});

////////////////////////////////////////////////////////////////
// Custom endpoints

router.post(
    '/register-exhibitor-user',
    auth.jwt,
    auth.accessLevel.superadmin,
    async function (req, res) {
        try {
            const exhibitor = await exhibitorModel.findOne({_id: req.body.exhibitorId});
            if (!exhibitor) {
                resH.send(res, null, false, 400, 'EXHIBITOR_DOES_NOT_EXIST');
            } else {
                const passFreeze = req.body.password;
                req.body.accountType = 'ADMIN';
                req.body.exhibitorCompany = exhibitor.name;
                const result = await auth.create.singleUser(req.body);
                if (result.success == true) {
                    EventBus.emit('tapcy2_exhibitor_user_register', {
                        firstName: req.body.firstName,
                        password: passFreeze,
                        email: req.body.username,
                        company: result.data.exhibitorCompany,
                    });
                    resH.send(res, result.data._id, true, 200, 'OK');
                } else {
                    resH.send(res, result.errors[0], false, 400, `Error: ${result.errors[0]}`);
                }
            }
        } catch (error) {
            logger.error(`Unable to register exhibitor user: ${error}`);
            resH.send(res, null, false, 400, `Unable to register exhibitor user: ${error}`);
        }
    }
);

router.post(
    '/:userId/cv',
    auth.jwt,
    auth.accessLevel.selfOrSuperadmin,
    global.upload.single('file'),
    async function (req, res) {
        try {
            const data = await CVController.uploadUserCv(req.params.userId, req.file, req.body);
            resH.send(res, data, true, 200, 'User CV successfully uploaded');
        } catch (error) {
            logger.error(`Unable to upload CV: ${error}`);
            resH.send(res, null, false, 400, `Unable to upload CV: ${error}`);
        }
    }
);

router.delete(
    '/:userId/cv/:cvId',
    auth.jwt,
    auth.accessLevel.selfOrSuperadmin,
    async function (req, res) {
        try {
            await CVController.markUserCvAsDeleted(req.params.userId, req.params.cvId);
            resH.send(res, null, true, 200, 'User CV successfully deleted');
        } catch (error) {
            logger.error(`Unable to delete CV: ${error}`);
            resH.send(res, null, false, 400, `Unable to delete CV: ${error}`);
        }
    }
);

module.exports = router;
