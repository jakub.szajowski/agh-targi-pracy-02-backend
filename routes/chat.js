const logger = require('winston');
const express = require('express');
const router = express.Router();
const messageModel = require('../models/message');
const resH = require('@enlighten1/express-response-helper');
const auth = require('@enlighten1/auth');

router.get('/recent/:channelId', auth.jwt, auth.accessLevel.user, async function (req, res) {
    try {
        let messages = await messageModel
            .find({channel: req.params.channelId})
            .limit(150)
            .sort({time: -1});
        if (messages) {
            messages = messages.sort((a, b) => a.time - b.time);
        }
        resH.send(res, messages);
    } catch (error) {
        logger.error(`Error while getting all chat messages: ${error}`);
        resH.send(res, null, false, 500, `Error while getting all chat messages: ${error}`);
    }
});

module.exports = router;
