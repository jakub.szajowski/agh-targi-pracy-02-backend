const moment = require('moment');
const linkifyHtml = require('linkifyjs/html');
const auth = require('@enlighten1/auth');

const mongoose = require('mongoose');
const MessageSchema = require('./schemas/message');

const MessageModel = mongoose.model('Message', MessageSchema);

MessageModel.handleNewMessage = async function (data) {
    try {
        const user = await auth.get.singleUser(data.authorId);
        if (user.isBlocked !== true) {
            let nameToDisp = `${user.firstName} ${user.lastName}`;
            if (user.accountType == 'ADMIN' && user.exhibitorCompany)
                nameToDisp = `${nameToDisp} – ${user.exhibitorCompany}`;
            const message = {
                channel: data.channel,
                authorName: nameToDisp,
                time: moment().valueOf(),
                isAdmin: user.accountType == 'ADMIN' || user.accountType == 'SUPERADMIN',
                isSuperAdmin: user.accountType == 'SUPERADMIN',
                content: linkifyHtml(data.content),
                authorId: data.authorId,
            };

            const newMsgDb = await this.create(message);
            return newMsgDb;
        } else {
            throw new Error('USER_BLOCKED');
        }
    } catch (error) {
        throw new Error(error);
    }
};

MessageModel.handleBlockUser = async function (data) {
    try {
        console.log(data);
        // TODO:
        // await userModel.updateOne({_id: data}, {isBlocked: true});
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = MessageModel;
