const mongoose = require('mongoose');
const JobOfferSchema = require('./schemas/jobOffer');

const JobOfferModel = mongoose.model('JobOffer', JobOfferSchema);

JobOfferModel.jobOfferExists = async function (jobOfferId) {
    try {
        const jobOffer = await this.findById(jobOfferId);
        if (jobOffer) return true;
        else return false;
    } catch (error) {
        throw new Error(error);
    }
};

// JobOfferModel.getExhibitorIdByJobOfferId = async function(jobOfferId) {
//     try {
//         const jobOffer = await this.findById(jobOfferId)
//         return jobOffer.exhibitorId;
//     } catch (error) {
//         throw new Error(error);
//     }
// }

module.exports = JobOfferModel;
