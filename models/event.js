const axios = require('axios');
const moment = require('moment');
const slugHelper = require('../helpers/slug-helper');
const mongoose = require('mongoose');
const EventSchema = require('./schemas/event');

const EventModel = mongoose.model('Event', EventSchema);

module.exports = EventModel;
