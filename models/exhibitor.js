const axios = require('axios');
const moment = require('moment');
const mongoose = require('mongoose');
const ExhibitorSchema = require('./schemas/exhibitor');
const fileHelper = require('../helpers/s3-upload-helper');

const ExhibitorModel = mongoose.model('Exhibitor', ExhibitorSchema);

ExhibitorModel.exhibitorExists = async function (exhibitorId) {
    try {
        const exhibitor = await this.findById(exhibitorId);
        if (exhibitor) return true;
        else return false;
    } catch (error) {
        throw new Error(error);
    }
};

ExhibitorModel.exhibitorLogoExists = async function (exhibitorId) {
    try {
        const exhibitor = await this.findById(exhibitorId);
        if (exhibitor.logo) return true;
        else return false;
    } catch (error) {
        throw new Error(error);
    }
};

ExhibitorModel.exhibitorImageExists = async function (exhibitorId, imageId) {
    try {
        const exhibitor = await this.findById(exhibitorId);
        let exists = false;

        for (let i = 0; i < exhibitor.images.length; i++) {
            if (exhibitor.images[i]._id == imageId) {
                exists = true;
            }
        }
        return exists;
    } catch (error) {
        throw new Error(error);
    }
};

ExhibitorModel.uploadExhibitorLogo = async function (exhibitorId, imageData) {
    try {
        const exhibitor = await this.findById(exhibitorId);

        exhibitor.logo = imageData;
        await exhibitor.save();
        return exhibitor.logo;
    } catch (error) {
        throw new Error(error);
    }
};

ExhibitorModel.deleteExhibitorLogo = async function (exhibitorId) {
    try {
        const exhibitor = await this.findById(exhibitorId);
        await fileHelper.delete(exhibitor.logo.fullSize.key);
        await fileHelper.delete(exhibitor.logo.medium.key);
        await fileHelper.delete(exhibitor.logo.thumb.key);

        exhibitor.logo = {
            fullSize: {
                key: null,
                url: null,
            },
            medium: {
                key: null,
                url: null,
            },
            thumb: {
                key: null,
                url: null,
            },
        };

        await exhibitor.save();
        return exhibitor.logo;
    } catch (error) {
        throw new Error(error);
    }
};

ExhibitorModel.uploadImage = async function (exhibitorId, imageData) {
    try {
        const exhibitor = await this.findById(exhibitorId);

        exhibitor.images.push(imageData);
        await exhibitor.save();
        return exhibitor.images.pop();
    } catch (error) {
        throw new Error(error);
    }
};

ExhibitorModel.spliceExhibitorImage = async function (exhibitorId, imageId) {
    try {
        const exhibitor = await this.findById(exhibitorId);

        for (let i = 0; i < exhibitor.images.length; i++) {
            if (exhibitor._id == exhibitorId && exhibitor.images[i]._id == imageId) {
                await fileHelper.delete(exhibitor.images[i].fullSize.key);
                await fileHelper.delete(exhibitor.images[i].medium.key);
                await fileHelper.delete(exhibitor.images[i].thumb.key);

                exhibitor.images.splice(i, 1);
            }
        }
        await exhibitor.save();
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = ExhibitorModel;
