const axios = require('axios');
const moment = require('moment');
const slugHelper = require('../helpers/slug-helper');
const mongoose = require('mongoose');
const jobApplicationSchema = require('./schemas/jobApplication');
const auth = require('@enlighten1/auth');

const JobApplicationModel = mongoose.model('jobApplication', jobApplicationSchema);

JobApplicationModel.jobApplicationExists = async function (applicationId) {
    try {
        const jobApplication = await this.findById(applicationId);
        if (jobApplication) return true;
        else return false;
    } catch (error) {
        throw new Error(error);
    }
};

JobApplicationModel.authorizeUser = async function (userId, applicationId) {
    try {
        const jobApplication = await this.findById(applicationId);
        if (jobApplication.userId === userId) return true;
        else return false;
    } catch (error) {
        throw new Error(error);
    }
};

JobApplicationModel.getCvUrlByUserIdAndCvId = async function (userId, cvId) {
    try {
        const user = await auth.get.singleUser(userId);
        for (let i = 0; i < user.cvs.length; i++) {
            if (user._id == userId && user.cvs[i]._id == cvId) {
                return user.cvs[i].url;
            }
        }
    } catch (error) {
        throw new Error(error);
    }
};

JobApplicationModel.freezeUserById = async function (userId) {
    const user = await auth.get.singleUser(userId);
    return {
        firstName: user.firstName,
        lastName: user.lastName,
        studentInfo: user.studentInfo,
        phone: user.phone,
        email: user.username,
        linkedIn: user.linkedIn,
    };
};

module.exports = JobApplicationModel;
