const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const s3FileSchema = new Schema({
    key: {
        type: String,
        default: null,
    },
    url: {
        type: String,
        default: null,
    },
});

module.exports = s3FileSchema;
