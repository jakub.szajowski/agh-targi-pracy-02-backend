const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const s3FileSchema = require('./s3File');

const ImageSchema = new Schema({
    fullSize: s3FileSchema,
    medium: s3FileSchema,
    thumb: s3FileSchema,
});

module.exports = ImageSchema;
