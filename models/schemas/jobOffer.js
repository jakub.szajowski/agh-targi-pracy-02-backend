const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const JobOfferSchema = new Schema({
    title: {
        type: String,
        default: null,
    },
    desc: {
        type: String,
        default: null,
    },
    slug: {
        type: String,
        default: null,
    },
    exhibitorId: {
        type: Schema.Types.ObjectId,
        default: null,
    },
    tags: [String],
    workplace: {
        type: String,
        default: null,
    },
});

module.exports = JobOfferSchema;
