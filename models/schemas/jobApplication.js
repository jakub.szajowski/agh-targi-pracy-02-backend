const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const jobApplicationSchema = new Schema({
    userId: {
        type: String,
        default: null,
    },
    exhibitorId: {
        type: String,
        default: null,
    },
    jobOfferId: {
        type: String,
        default: null,
    },
    cvId: {
        type: String,
        default: null,
    },
    cvUrl: {
        type: String,
        default: null,
    },
    userFreeze: {
        firstName: {type: String, default: null},
        lastName: {type: String, default: null},
        studentInfo: {
            status: {type: String, default: null},
            major: {type: String, default: null},
        },
        email: {type: String, default: null},
        phone: {type: String, default: null},
        linkedIn: {type: String, default: null},
    },
    message: {
        type: String,
        default: null,
    },
});

module.exports = jobApplicationSchema;
