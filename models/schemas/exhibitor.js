const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ImageSchema = require('./subschemas/image');

const ExhibitorSchema = new Schema({
    name: {
        type: String,
        default: null,
    },
    slug: {
        type: String,
        default: null,
    },
    logo: ImageSchema,
    tags: [String],
    workplaces: [{name: String}],
    desc: {
        type: String,
        default: null,
    },
    shortDesc: {
        type: String,
        default: null,
    },
    fundraiser: {
        type: Boolean,
        default: null,
    },
    websites: [{url: String, name: String}],
    socialMedia: {
        facebook: {type: String, default: null},
        instagram: {type: String, default: null},
        youtube: {type: String, default: null},
        tiktok: {type: String, default: null},
        twitter: {type: String, default: null},
        other: {type: String, default: null},
        linkedIn: {type: String, default: null},
    },
    calendly: {
        type: String,
        default: null,
    },
    appointment: {
        teamsUrl: {type: String, default: null},
        calendarUrl: {type: String, default: null},
        calendarCode: {type: String, default: null},
    },
    youtubeVideos: [{url: String}],
    images: [ImageSchema],
});

module.exports = ExhibitorSchema;
