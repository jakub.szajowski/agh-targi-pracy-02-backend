const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const EventSchema = new Schema({
    title: {
        type: String,
        default: null,
    },
    host: {
        type: String,
        default: null,
    },
    slug: {
        type: String,
        default: null,
    },
    exhibitorId: {
        type: Schema.Types.ObjectId,
        default: null,
    },
    desc: {
        type: String,
        default: null,
    },
    startTime: {
        type: Number,
        default: null,
    },
    finishTime: {
        type: Number,
        default: null,
    },
    isActive: {
        type: Boolean,
        default: false,
    },
    workshops: {
        type: Boolean,
        default: true,
    },
    link: {
        type: String,
        default: null,
    },
    streamyardLink: {
        type: String,
        default: null,
    },
    hostContact: {
        type: String,
        default: null,
    },
});

module.exports = EventSchema;
