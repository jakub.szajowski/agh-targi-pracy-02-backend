const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    time: {
        type: Number,
        default: null,
    },
    authorId: {
        type: Schema.Types.ObjectId,
        default: null,
    },
    authorName: {
        type: String,
        default: '',
    },
    content: {
        type: String,
        default: '',
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    isSuperAdmin: {
        type: Boolean,
        default: false,
    },
    channel: {
        type: String,
        default: null,
    },
});

module.exports = MessageSchema;
