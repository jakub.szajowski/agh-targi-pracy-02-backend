const auth = require('@enlighten1/auth');
const accessLevelMiddleware = new Object();

accessLevelMiddleware.staffOrSuperadmin = async function (req, res, next) {
    const user = await auth.get.singleUser(req.user._id);

    if (user.exhibitorId == req.params.exhibitorId || req.user.accountType == 'SUPERADMIN') {
        next();
    } else {
        res.status(403).send('Forbidden');
    }
};

module.exports = accessLevelMiddleware;
