const slugify = require('slugify');

const getSlug = function (arg) {
    if (arg) {
        let str = slugify(arg, {lower: true, remove: /[*+~#.,–()'"!/?:@]/g});
        return str;
    } else {
        return '';
    }
};

module.exports = {getSlug: getSlug};
