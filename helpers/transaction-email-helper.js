const logger = require('winston');
const axios = require('axios');
const EventBus = require('@enlighten1/enl-event-bus');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

function notifyUserRegistration(firstName, email) {
    return new Promise((resolve, reject) => {
        const msg = {
            to: email,
            from: {
                email: 'noreply@platforma.targi.agh.edu.pl',
                name: 'Targi Pracy AGH',
            },
            templateId: 'd-fbbc464e29274da4abd3a514263d4334',
            dynamic_template_data: {
                first_name_arg: firstName,
                email_arg: email,
            },
        };
        sgMail
            .send(msg)
            .then(() => {
                resolve();
            })
            .catch(error => {
                reject(error.message);
            });
    });
}

function notifyPasswordResetInit(email, tokenString) {
    return new Promise((resolve, reject) => {
        const msg = {
            to: email,
            from: {
                email: 'noreply@platforma.targi.agh.edu.pl',
                name: 'Targi Pracy AGH',
            },
            templateId: 'd-f096380e8df8414495d860a316cc75c9',
            dynamicTemplateData: {
                link_arg: `${global.config.web_app_url}/nowe-haslo/${tokenString}`,
            },
        };
        sgMail
            .send(msg)
            .then(() => {
                resolve();
            })
            .catch(error => {
                reject(error.message);
            });
    });
}

function notifyPasswordResetFinale(email) {
    return new Promise((resolve, reject) => {
        const msg = {
            to: email,
            from: {
                email: 'noreply@platforma.targi.agh.edu.pl',
                name: 'Targi Pracy AGH',
            },
            templateId: 'd-c568ae0a575b47a88f9e1f317f1fd572',
            dynamic_template_data: {
                email_arg: email,
            },
        };
        sgMail
            .send(msg)
            .then(() => {
                resolve();
            })
            .catch(error => {
                reject(error.message);
            });
    });
}

function notifyUserExhibitorRegistration(firstName, email, password, company) {
    return new Promise((resolve, reject) => {
        logger.info(`running 'notifyUserExhibitorRegistration' function, ${email}`);
        const msg = {
            to: email,
            from: {
                email: 'noreply@platforma.targi.agh.edu.pl',
                name: 'Targi Pracy AGH',
            },
            templateId: 'd-3f9274ab505c4c71a5c71a2be26e55f6',
            dynamic_template_data: {
                first_name_arg: firstName,
                email_arg: email,
                password_arg: password,
                company_arg: company,
            },
        };
        sgMail
            .send(msg)
            .then(() => {
                logger.debug('Notification email successfully sent to: ' + email);
                resolve();
            })
            .catch(error => {
                logger.error(error.message);
                reject(error.message);
            });
    });
}

EventBus.on('auth_register', data => {
    if (!global.config.is_test) notifyUserRegistration(data.user.firstName, data.user.username);
});

EventBus.on('auth_password_reset_init', data => {
    if (!global.config.is_test)
        notifyPasswordResetInit(data.user.username, data.token.tokenString);
});

EventBus.on('auth_perform_password_reset', data => {
    if (!global.config.is_test) notifyPasswordResetFinale(data.user.username);
});

EventBus.on('tapcy2_exhibitor_user_register', data => {
    if (!global.config.is_test)
        notifyUserExhibitorRegistration(data.firstName, data.email, data.password, data.company);
});
