const logger = require('winston');
const axios = require('axios');
const auth = require('@enlighten1/auth');

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');
const defaultTemplateId = 'd-a02ff4722c1e4f05996c319508149a3b';

const smsApiToken = 'cFZQPGhJurC3FXxinxz0lkAJBXxh52u1qe3AajiU';
const smsApiUrl = 'https://api.smsapi.pl';

let helper = {};

// From Stack Overflow WTF IDK
const array_chunks = (array, chunk_size) =>
    Array(Math.ceil(array.length / chunk_size))
        .fill()
        .map((_, index) => index * chunk_size)
        .map(begin => array.slice(begin, begin + chunk_size));

helper.sendEmailToAllDefault = async function (title, body) {
    try {
        logger.warn(`Sending email to all users via default template.`);

        const allUsersFromDb = await auth.get.allUsers();
        const allUsersFlat = JSON.parse(JSON.stringify(allUsersFromDb));
        const users = allUsersFlat.filter(u => u.accountType !== 'ADMIN').map(u => u.username);

        const CHUNK_SIZE = 1000;
        const usersChunks = array_chunks(users, CHUNK_SIZE);

        for (let i = 0; i < usersChunks.length; i++) {
            const msg = {
                to: usersChunks[i],
                subject: title,
                from: {
                    email: 'noreply@platforma.targi.agh.edu.pl',
                    name: 'Targi Pracy AGH',
                },
                templateId: defaultTemplateId,
                dynamic_template_data: {
                    email_content_arg: body,
                    subject: title,
                },
            };

            await sgMail.sendMultiple(msg);
            logger.debug(`Chunk ${i + 1}/${usersChunks.length} sent.`);
        }

        logger.info(`Email to all users via default template has been sent.`);

        // console.log(usersChunks);
    } catch (error) {
        throw new Error(error);
    }
};

helper.sendEmailToAllTemplate = async function (title, templateId) {
    try {
        logger.warn(`Sending email to all users via custom template. Template id: ${templateId}`);

        const allUsersFromDb = await auth.get.allUsers();
        const allUsersFlat = JSON.parse(JSON.stringify(allUsersFromDb));
        const users = allUsersFlat.filter(u => u.accountType !== 'ADMIN').map(u => u.username);

        const CHUNK_SIZE = 1000;
        const usersChunks = array_chunks(users, CHUNK_SIZE);

        for (let i = 0; i < usersChunks.length; i++) {
            const msg = {
                to: usersChunks[i],
                subject: title,
                from: {
                    email: 'noreply@platforma.targi.agh.edu.pl',
                    name: 'Targi Pracy AGH',
                },
                templateId: templateId,
                dynamic_template_data: {
                    subject: title,
                },
            };

            await sgMail.sendMultiple(msg);
            logger.debug(`Chunk ${i + 1}/${usersChunks.length} sent.`);
        }

        logger.info(`Email to all users via custom template has been sent.`);
    } catch (error) {
        throw new Error(error);
    }
};

helper.sendSMSToAll = async function (text) {
    try {
        if (!text) throw new Error('Text is empty');

        logger.warn(`Sending SMS to all users.`);

        const allUsersFromDb = await auth.get.allUsers();
        const allUsersFlat = JSON.parse(JSON.stringify(allUsersFromDb));
        const users = allUsersFlat.map(u => u.phone);
        const phones = users.filter(u => u);

        const config = {
            headers: {Authorization: `Bearer ${smsApiToken}`},
        };

        const body = {
            to: phones,
            from: 'AGH',
            encoding: 'utf-8',
            message: text,
        };

        const response = await axios.post(`${smsApiUrl}/sms.do`, body, config);

        logger.info(`SMS to all users sent.`);
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = helper;
