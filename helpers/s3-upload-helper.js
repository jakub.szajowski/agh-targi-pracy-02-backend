const fs = require('fs');
const mime = require('mime-types');

const helper = new Object();

/**
 * Uploads file to S3 bucket
 * @param {String} localPath - Path to file on localfilesystem
 * @param {String} remotePath - Remote path to file os S3 bucket under which the file will be uploaded
 */

helper.upload = async function (localPath, remotePath, mimetype) {
    return new Promise((resolve, reject) => {
        const bucket = global.config.s3_bucket + global.config.s3_bucket_specifier;

        const fileContents = fs.readFileSync(localPath);

        const uploadParams = {
            Body: fileContents,
            Bucket: bucket,
            Key: remotePath,
            ContentType: mimetype || mime.lookup(localPath),
            ACL: 'public-read',
        };

        global.s3.upload(uploadParams, function (error, data) {
            if (error) {
                reject(error);
            } else {
                resolve({
                    key: data.Key,
                    url: data.Location,
                });
            }
        });
    });
};

helper.delete = async function (remotePath, bucket = global.config.s3_bucket) {
    return new Promise((resolve, reject) => {
        const deleteParams = {
            Bucket: bucket,
            Key: remotePath,
        };

        global.s3.deleteObject(deleteParams, function (error, data) {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
};

module.exports = helper;
