const sharp = require('sharp');
const probe = require('probe-image-size');
const fs = require('fs');

let helper = {};

function resize(inputPath, outputPath, targetWidth) {
    return new Promise((resolve, reject) => {
        const data = fs.readFileSync(inputPath);
        const dimensions = probe.sync(data);
        const imageHeight = dimensions.height;
        const imageWidth = dimensions.width;

        const params = {
            width: targetWidth,
            height: Math.round((targetWidth / imageWidth) * imageHeight),
        };

        sharp(inputPath)
            .resize(params)
            .toFile(outputPath)
            .then(newFile => {
                resolve(newFile);
            })
            .catch(error => {
                reject(error);
            });
    });
}

function modifyPath(path, fileNameSufix) {
    const extensionWithDot = path.substring(path.length - 4, path.length);
    const pathWithoutExtensionAndDot = path.substring(0, path.length - 4);
    const newPath = pathWithoutExtensionAndDot + `-${fileNameSufix}` + extensionWithDot;
    return newPath;
}

helper.generateMedium = async function (inPath) {
    try {
        const generatedOutPath = modifyPath(inPath, 'medium');
        await resize(inPath, generatedOutPath, 1000);
        return generatedOutPath;
    } catch (error) {
        throw new Error(error);
    }
};

helper.generateThumb = async function (inPath) {
    try {
        const generatedOutPath = modifyPath(inPath, 'thumb');
        await resize(inPath, generatedOutPath, 500);
        return generatedOutPath;
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = helper;
