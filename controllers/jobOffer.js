const slugHelper = require('../helpers/slug-helper');
const jobOfferModel = require('../models/jobOffers');

const controller = new Object();

controller.getAll = async function () {
    try {
        const data = await jobOfferModel.find({});
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.getById = async function (id) {
    try {
        const data = await jobOfferModel.findById(id);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.create = async function (data_arg) {
    try {
        data_arg.slug = slugHelper.getSlug(data_arg.title);
        const data = await jobOfferModel.create(data_arg);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.updateById = async function (id, data_arg) {
    try {
        const data = await jobOfferModel.findByIdAndUpdate(id, data_arg, {new: true});
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.deleteById = async function (id) {
    try {
        const data = await jobOfferModel.findByIdAndRemove(id);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = controller;
