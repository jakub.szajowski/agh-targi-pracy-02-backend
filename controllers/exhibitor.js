const fs = require('fs');
const EventBus = require('@enlighten1/enl-event-bus');
const exhibitorModel = require('../models/exhibitor');
const imageResizeHelper = require('../helpers/image-resize');
const fileHelper = require('../helpers/s3-upload-helper');
const slugHelper = require('../helpers/slug-helper');
const mime = require('mime-types');

const controller = new Object();

controller.getAll = async function () {
    try {
        const data = await exhibitorModel.find({});
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.getById = async function (id) {
    try {
        const data = await exhibitorModel.findById(id);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.create = async function (exhibitorData) {
    try {
        exhibitorData.slug = slugHelper.getSlug(exhibitorData.name);
        const data = await exhibitorModel.create(exhibitorData);
        EventBus.emit('exhibitorCreate', data);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.updateById = async function (id, exhibitorData) {
    try {
        const data = await exhibitorModel.findByIdAndUpdate(id, exhibitorData, {new: true});
        EventBus.emit('exhibitorUpdate', data);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.uploadExhibitorLogo = async (exhibitorId, file) => {
    try {
        const exhibitorExists = await exhibitorModel.exhibitorExists(exhibitorId);
        if (!exhibitorExists) throw new Error('EXHIBITOR_NOT_EXIST');

        const logoExists = await exhibitorModel.exhibitorLogoExists(exhibitorId);

        if (logoExists) {
            await exhibitorModel.deleteExhibitorLogo(exhibitorId);
        }

        // Full size
        const localPathFullSize = `uploads/${file.filename}`;
        const mimeType = mime.lookup(localPathFullSize);
        const remotePathFullSize = await fileHelper.upload(
            localPathFullSize,
            `exhibitors/${exhibitorId}/logo/fullsize-${file.filename}`,
            mimeType
        );

        // Medium
        const localPathMedium = await imageResizeHelper.generateMedium(file.path);
        const remotePathMedium = await fileHelper.upload(
            localPathMedium,
            `exhibitors/${exhibitorId}/logo/medium-${file.filename}`,
            mimeType
        );

        // Thumb
        const localPathThumb = await imageResizeHelper.generateThumb(file.path);
        const remotePathThumb = await fileHelper.upload(
            localPathThumb,
            `exhibitors/${exhibitorId}/logo/thumb-${file.filename}`,
            mimeType
        );

        const imageData = {
            fullSize: remotePathFullSize,
            medium: remotePathMedium,
            thumb: remotePathThumb,
        };

        const data = await exhibitorModel.uploadExhibitorLogo(exhibitorId, imageData);

        fs.unlinkSync(file.path);
        fs.unlinkSync(localPathMedium);
        fs.unlinkSync(localPathThumb);

        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.deleteExhibitorLogo = async function (exhibitorId) {
    try {
        const exhibitorExists = await exhibitorModel.exhibitorExists(exhibitorId);
        if (!exhibitorExists) throw new Error('EXHIBITOR_NOT_EXIST');

        const logoExists = await exhibitorModel.exhibitorLogoExists(exhibitorId);
        if (!logoExists) throw new Error('LOGO_FILE_NOT_EXIST');

        const data = await exhibitorModel.deleteExhibitorLogo(exhibitorId);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.uploadExhibitorImage = async function (exhibitorId, file) {
    try {
        const exhibitorExists = await exhibitorModel.exhibitorExists(exhibitorId);
        if (!exhibitorExists) throw new Error('EXHIBITOR_NOT_EXIST');

        // Full size
        const localPathFullSize = `uploads/${file.filename}`;
        const mimeType = mime.lookup(localPathFullSize);

        const remotePathFullSize = await fileHelper.upload(
            localPathFullSize,
            `exhibitors/${exhibitorId}/images/fullsize-${file.filename}`,
            mimeType
        );

        // Medium
        const localPathMedium = await imageResizeHelper.generateMedium(file.path);
        const remotePathMedium = await fileHelper.upload(
            localPathMedium,
            `exhibitors/${exhibitorId}/images/medium-${file.filename}`,
            mimeType
        );

        // Thumb
        const localPathThumb = await imageResizeHelper.generateThumb(file.path);
        const remotePathThumb = await fileHelper.upload(
            localPathThumb,
            `exhibitors/${exhibitorId}/images/thumb-${file.filename}`,
            mimeType
        );

        const imageData = {
            fullSize: remotePathFullSize,
            medium: remotePathMedium,
            thumb: remotePathThumb,
        };

        const data = await exhibitorModel.uploadImage(exhibitorId, imageData);

        fs.unlinkSync(file.path);
        fs.unlinkSync(localPathMedium);
        fs.unlinkSync(localPathThumb);

        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.deleteExhibitorImage = async function (exhibitorId, imageId) {
    try {
        const exhibitorExists = await exhibitorModel.exhibitorExists(exhibitorId);
        if (!exhibitorExists) throw new Error('EXHIBITOR_NOT_EXIST');

        const imageExists = await exhibitorModel.exhibitorImageExists(exhibitorId, imageId);
        if (!imageExists) throw new Error('IMAGE_NOT_EXIST');

        const data = await exhibitorModel.spliceExhibitorImage(exhibitorId, imageId);

        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.deleteById = async function (id) {
    try {
        const data = await exhibitorModel.findByIdAndRemove(id);
        EventBus.emit('exhibitorDelete', data);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = controller;
