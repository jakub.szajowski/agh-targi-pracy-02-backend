const fs = require('fs');
const fileHelper = require('../helpers/s3-upload-helper');
const applicationModel = require('../models/jobApplication');
const jobOfferModel = require('../models/jobOffers');
const exhibitorModel = require('../models/exhibitor');
const auth = require('@enlighten1/auth');
const {application} = require('express');

const controller = new Object();

controller.getByExhibitorId = async function (exhibitorId) {
    try {
        const data = await applicationModel.find({exhibitorId: exhibitorId});
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.getByUserId = async function (userId) {
    try {
        const data = await applicationModel.find({userId: userId});
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.create = async function (data) {
    try {
        const exhibitorExists = await exhibitorModel.exhibitorExists(data.exhibitorId);
        if (!exhibitorExists)
            return {status: 400, success: false, data: null, message: 'exhibitor not exist.'};

        if (data.jobOfferId) {
            const jobOfferExists = await jobOfferModel.jobOfferExists(data.jobOfferId);
            if (!jobOfferExists)
                return {status: 400, success: false, data: null, message: 'job offer not exist.'};
        }

        //assembling data
        //data.exhibitorId = await jobOfferModel.getExhibitorIdByJobOfferId(data.jobOfferId)
        data.cvUrl = await applicationModel.getCvUrlByUserIdAndCvId(data.userId, data.cvId);
        data.userFreeze = await applicationModel.freezeUserById(data.userId);

        //creating DB record
        const newJobApplication = await applicationModel.create(data);
        return {
            status: 200,
            success: true,
            data: newJobApplication,
            message: 'job application successfully created.',
        };
    } catch (error) {
        throw new Error(error);
    }
};

controller.update = async function (userId, applicationId, data) {
    try {
        const userIsAuthor = applicationModel.authorizeUser(userId, applicationId);
        if (!userIsAuthor) {
            return {
                status: 401,
                success: false,
                data: null,
                message: 'Unauthorized',
            };
        }

        const updatedJobApplication = await applicationModel.findByIdAndUpdate(
            applicationId,
            data,
            {new: true}
        );
        return {
            status: 200,
            success: true,
            data: updatedJobApplication,
            message: 'job application successfully updated.',
        };
    } catch (error) {
        throw new Error(error);
    }
};

controller.delete = async function (userId, applicationId) {
    try {
        // const jobApplicationExists = applicationModel.jobApplicationExists(applicationId)
        // if (!jobApplicationExists) throw new Error('JOB_APPLICATION_NOT_EXIST');

        const userIsAuthor = await applicationModel.authorizeUser(userId, applicationId);
        if (!userIsAuthor) {
            return {
                status: 401,
                success: false,
                data: null,
                message: 'Unauthorized',
            };
        }

        await applicationModel.findByIdAndDelete(applicationId);
        return {
            status: 200,
            success: true,
            data: null,
            message: 'job application successfully deleted.',
        };
    } catch (error) {
        throw new Error(error);
    }
};

controller.getNumberPerExhibitor = async function () {
    try {
        const applications = await applicationModel.find({});
        const exhibitors = await exhibitorModel.find({});
        return exhibitors.map(exhibitor => {
            return {
                exhibitorId: exhibitor._id,
                exhibitorName: exhibitor.name,
                applicationsCount: applications.filter(
                    application => application.exhibitorId == exhibitor._id
                ).length,
            };
        });
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = controller;
