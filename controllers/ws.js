const EventBus = require('@enlighten1/enl-event-bus');
const logger = require('winston');
const messageModel = require('../models/message');
const jwt = require('jsonwebtoken');

global.io.on('connection', function (socket) {
    logger.silly('New connection from user!');

    socket.on('message', data => {
        jwt.verify(data.authToken, 'secret', function (err, decoded) {
            if (decoded) {
                messageModel
                    .handleNewMessage(data)
                    .then(toBroadcast => {
                        socket.emit('message', toBroadcast);
                        socket.broadcast.emit('message', toBroadcast);
                    })
                    .catch(error => {
                        logger.error(error);
                    });
            } else {
                logger.error(`Bad token provided in ws message event. ${err}`);
            }
        });
    });

    socket.on('deleteMsg', data => {
        logger.warn('Delete message.');
        console.log(data);
        messageModel
            .deleteOne({_id: data})
            .then(() => {
                socket.emit('deleteMsg', data);
                socket.broadcast.emit('deleteMsg', data);
            })
            .catch(error => {
                logger.error(error);
            });
    });

    socket.on('blockUser', data => {
        logger.warn('blockUser', data);
        messageModel
            .handleBlockUser(data)
            .then(() => {
                socket.emit('blockUser', data);
                socket.broadcast.emit('blockUser', data);
            })
            .catch(error => {
                logger.error(error);
            });
    });
});

EventBus.on('eventCreate', data => {
    global.io.emit('eventCreate', data);
});

EventBus.on('eventUpdate', data => {
    global.io.emit('eventUpdate', data);
    if (!global.config.is_test) logger.silly('eventUpdate emited');
});

EventBus.on('eventDelete', id => {
    global.io.emit('eventDelete', id);
});

EventBus.on('exhibitorCreate', data => {
    global.io.emit('exhibitorCreate', data);
});

EventBus.on('exhibitorUpdate', data => {
    global.io.emit('exhibitorUpdate', data);
});

EventBus.on('exhibitorDelete', id => {
    global.io.emit('exhibitorDelete', id);
});
