const fs = require('fs');
const fileHelper = require('../helpers/s3-upload-helper');
const auth = require('@enlighten1/auth');

const controller = new Object();

controller.uploadUserCv = async function (userId, file, data) {
    try {
        const user = await auth.get.singleUser(userId);
        if (!user) throw new Error('USER_NOT_FOUND');

        const localPath = `uploads/${file.filename}`;
        const remotePath = await fileHelper.upload(
            localPath,
            `users/${userId}/cvs/${file.filename}`
        );

        const cvData = {
            key: remotePath.key,
            url: remotePath.url,
            name: data.fileName || '',
            isDeleted: false,
        };

        user.cvs.push(cvData);

        const userUpdated = await auth.update.singleUser(userId, {cvs: user.cvs});

        fs.unlinkSync(file.path);

        return userUpdated.cvs.pop();
    } catch (error) {
        throw new Error(error);
    }
};

controller.markUserCvAsDeleted = async function (userId, cvId) {
    try {
        const user = await auth.get.singleUser(userId);
        if (!user) throw new Error('USER_NOT_FOUND');

        const cvExists = user.cvs.some(cv => `${cv._id}` === cvId);
        if (!cvExists) throw new Error('CV_NOT_FOUND');

        const index = user.cvs.findIndex(cv => `${cv._id}` === cvId);
        user.cvs[index].isDeleted = true;

        const userUpdated = await auth.update.singleUser(userId, {cvs: user.cvs});
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = controller;
