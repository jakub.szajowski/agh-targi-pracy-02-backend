const EventBus = require('@enlighten1/enl-event-bus');
const moment = require('moment');
const slugHelper = require('../helpers/slug-helper');
const eventModel = require('../models/event');

const controller = new Object();

controller.getAll = async function () {
    try {
        const data = await eventModel.find({});
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.getById = async function (id) {
    try {
        const data = await eventModel.findById(id);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.create = async function (data_arg) {
    try {
        data_arg.slug = slugHelper.getSlug(data_arg.title);
        const data = await eventModel.create(data_arg);
        EventBus.emit('eventCreate', data);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.updateById = async function (id, data_arg) {
    try {
        const data = await eventModel.findByIdAndUpdate(id, data_arg, {new: true});
        EventBus.emit('eventUpdate', data);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.deleteById = async function (id) {
    try {
        const data = await eventModel.findByIdAndRemove(id);
        EventBus.emit('eventDelete', id);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

controller.updateActiveEvents = async function () {
    try {
        const now = moment();

        let counter = 0;

        const allEvents = await eventModel.find({});

        for (let i = 0; i < allEvents.length; i++) {
            let changed = false;
            let newActive = allEvents[i].isActive;

            /* REGULAR EVENTS */
            // Start every not active event (not workshop) event 5 min before it starts
            if (
                now.valueOf() >= allEvents[i].startTime - 5 * 60 * 1000 &&
                allEvents[i].isActive === false &&
                now.valueOf() <= allEvents[i].finishTime &&
                allEvents[i].workshops === false
            ) {
                changed = true;
                newActive = true;
            }

            // Stop every active event (not workshop) after it's finish time + 15 min
            if (
                now.valueOf() >= allEvents[i].finishTime + 15 * 60 * 1000 &&
                allEvents[i].isActive === true &&
                allEvents[i].workshops === false
            ) {
                changed = true;
                newActive = false;
            }

            // Start every not active workshop event 5 min before it starts
            if (
                now.valueOf() >= allEvents[i].startTime - 5 * 60 * 1000 &&
                allEvents[i].isActive === false &&
                now.valueOf() <= allEvents[i].startTime + 65 * 60 * 1000 &&
                allEvents[i].workshops === true
            ) {
                changed = true;
                newActive = true;
            }

            // Stop every active workshop 65 min after it's start
            if (
                now.valueOf() >= allEvents[i].startTime + 65 * 60 * 1000 &&
                allEvents[i].isActive === true &&
                allEvents[i].workshops === true
            ) {
                changed = true;
                newActive = false;
            }

            // Handle only changed events, skip unchanged
            if (changed) {
                const result = await this.updateById(allEvents[i]._id, {isActive: newActive});
                counter++;
            }
        }

        return counter;
    } catch (error) {
        throw new Error(error);
    }
};

module.exports = controller;
