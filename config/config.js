const config = {
    port: process.env.PORT || 3300,
    is_test: process.env.IS_TEST || false,
    env: process.env.NODE_ENV || 'development',
    useRedis: process.env.USE_REDIS === 'true' || false,
    redisHost: process.env.REDIS_HOST || null,
    redisPort: process.env.REDIS_PORT || null,
    web_app_url: process.env.WEB_APP_URL || 'http://localhost-web-app-invalid',
    admin_app_url: process.env.ADMIN_APP_URL || 'http://localhost-admin-invalid',
    s3_bucket: process.env.S3_BUCKET || 'enlighten-space',
    s3_bucket_specifier: process.env.S3_BUCKET_SPECIFIER || '/agh-tapcy-2-dev',
    s3_endpoint: process.env.S3_ENDPOINT || 'fra1.digitaloceanspaces.com',
    s3_accessKeyId: process.env.S3_ACCESS_KEY_ID || 'SHTKZZEI7UDGDZ3PJUG2',
    s3_secretAccessKey:
        process.env.S3_SECRET_ACCESS_KEY || 'vfeeS3d/AodtqXXVLW6Z2iOck8bbMIjjyIW3o7X7++k',
};

module.exports = config;
