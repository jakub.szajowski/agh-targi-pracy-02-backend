const config = require('./config');
const mongoose = require('mongoose');
const logger = require('winston');

const mongoURL = function () {
    if (global.config.env === 'production')
        return 'mongodb+srv://ENL:TsPNY8oAEt4JAm2X@cluster0.gue58.mongodb.net/agh-tapcy-2-production?retryWrites=true&w=majority';
    else if (global.config.is_test)
        return 'mongodb+srv://ENL:TsPNY8oAEt4JAm2X@cluster0.gue58.mongodb.net/agh-tapcy-2-test?retryWrites=true&w=majority';
    else
        return 'mongodb+srv://ENL:TsPNY8oAEt4JAm2X@cluster0.gue58.mongodb.net/agh-tapcy-2-development?retryWrites=true&w=majority';
};

mongoose.connect(mongoURL(), {useNewUrlParser: true, useUnifiedTopology: true}, function (err) {
    if (err) {
        logger.error('Failed to estabilish connection with database.');
        logger.error(err.message);
        logger.error(`Database connection address: ${mongoURL()}`);
    }
});

var db = mongoose.connection;

db.once('open', function () {
    if (!global.config.is_test) {
        logger.info('Estabilished connection with database');
        logger.info(`Database connection address: ${mongoURL()}`);
    }
});

mongoose.Promise = global.Promise;
module.exports = mongoose;
