const authConfig = {
    passwordMinLength: 8,
    tokenExpirationTime: '7d',
    userCustomSchema: {
        firstName: {
            type: String,
            default: '',
        },
        lastName: {
            type: String,
            default: '',
        },
        phone: {
            type: String,
            default: '',
        },
        studentInfo: {
            status: {
                type: String,
                default: null,
            },
            major: {
                type: String,
                default: null,
            },
        },
        isBlocked: {
            type: Boolean,
            default: false,
        },
        adminOfChannel: {
            type: String,
            default: null,
        },
        exhibitorCompany: {
            type: String,
            default: null,
        },
        exhibitorId: {
            type: String,
            default: null,
        },
        cvs: [
            {
                key: {
                    type: String,
                    default: null,
                },
                url: {
                    type: String,
                    default: null,
                },
                name: {
                    type: String,
                    default: null,
                },
                isDeleted: {
                    type: Boolean,
                    default: false,
                },
            },
        ],
        linkedIn: {
            type: String,
            default: null,
        },
    },
};

module.exports = authConfig;
