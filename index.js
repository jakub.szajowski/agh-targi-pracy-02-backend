// npm dependecies
const dotenv = require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const app = express();
const logger = require('winston');
const socket = require('socket.io');
const redisAdapter = require('socket.io-redis');
const AWS = require('aws-sdk');

const http = require('http').Server(app);

// config
global.config = require('./config/config');

//multer
const multer = require('multer');
const fileStorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads');
    },
    filename: (req, file, cb) => {
        // cb(null, Date.now() + '_' + file.originalname);
        const {originalname} = file;
        const fileExtension = (originalname.match(/\.+[\S]+$/) || [])[0];
        cb(null, `${Date.now()}${fileExtension}`);
    },
});

global.upload = multer({storage: fileStorageEngine});

// S3
const spacesEndpoint = new AWS.Endpoint(global.config.s3_endpoint);
global.s3 = new AWS.S3({
    endpoint: spacesEndpoint,
    accessKeyId: global.config.s3_accessKeyId,
    secretAccessKey: global.config.s3_secretAccessKey,
});

// Imports the Google Cloud client library for Winston
const {LoggingWinston} = require('@google-cloud/logging-winston');

const loggingWinston = new LoggingWinston({
    projectId: 'logs-286819',
    keyFilename: './logging/serviceAccount.json',
    logName: 'agh-tapcy-v2-prod',
});

let loggerConfig;
if (global.config.env === 'production' || global.config.env === 'staging') {
    loggerConfig = {
        level: 'debug',
        transports: [loggingWinston],
    };
} else {
    loggerConfig = {
        level: 'silly',
        transports: [
            new logger.transports.Console({
                format: logger.format.simple(),
            }),
        ],
    };
}

logger.configure(loggerConfig);

// database
const mongoose = require('./config/database');

const auth = require('@enlighten1/auth');
const authConfig = require('./config/authConfig');
auth.configure(authConfig);

// middleware
app.disable('etag');
app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// routes
const users = require('./routes/users');
const exhibitors = require('./routes/exhibitors');
const events = require('./routes/events');
const jobOffers = require('./routes/jobOffers');
const jobApplications = require('./routes/jobApplications');
const chat = require('./routes/chat');
const comm = require('./routes/comm');

// routes usage
app.use('/users', users);
app.use('/exhibitors', exhibitors);
app.use('/events', events);
app.use('/job-offers', jobOffers);
app.use('/job-applications', jobApplications);
app.use('/chat', chat);
app.use('/comm', comm);

// helpers
const transactionEmails = require('./helpers/transaction-email-helper');

app.get('/', function (req, res) {
    res.status(418).send('Hello, this is agh-targi-pracy-2-backend. How can I help you?');
});

let server = http.listen(config.port, function () {
    logger.info(
        `agh-targi-pracy-2-event-backend server started at port ${global.config.port}. Environment: ${global.config.env}`
    );
    if (global.config.env == 'development')
        logger.info(`You can access the API: http://localhost:${global.config.port}`);
    if (global.config.is_test) logger.info(`This is a test environment.`);
});

let stop = function () {
    server.close();
};

// socket
global.io = socket(server, {
    cors: {
        origin: '*',
        transports: ['websocket'],
    },
});

if (global.config.useRedis) {
    logger.info(
        `Using Redis. Redis host: ${global.config.redisHost}, Redis port: ${global.config.redisPort}`
    );
    const redisAdapterConfig = {
        host: global.config.redisHost,
        port: global.config.redisPort,
    };
    global.io.adapter(redisAdapter(redisAdapterConfig));
} else {
    logger.info(`Not using Redis.`);
}

const wsController = require('./controllers/ws');

module.exports = server;
module.exports.stop = stop;
